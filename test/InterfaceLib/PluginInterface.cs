﻿using System;

namespace InterfaceLib
{
    /// <summary>
    /// 插件接口
    /// </summary>
    public abstract class PluginInterface //: MarshalByRefObject
    {
        /// <summary>
        /// 加载插件后调用，插件必须实现的方法
        /// </summary>
        public abstract void Load();

        /// <summary>
        /// 卸载插件前调用，插件必须实现的方法
        /// </summary>
        public abstract void Unload();

        /// <summary>
        /// 第一个功能方法，可选实现
        /// </summary>
        /// <returns></returns>
        public virtual string Method1()
        {
            return null;
        }

        /// <summary>
        /// 第二个功能方法，可选实现
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        public virtual string Method2(string from)
        {
            return from;
        }
    }
}
