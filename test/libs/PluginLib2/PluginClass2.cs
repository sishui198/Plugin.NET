﻿using InterfaceLib;
using System;

namespace PluginLib2
{
    public class PluginClass2 : PluginInterface
    {
        public override void Load()
        {
            Console.WriteLine("Load" + this.GetType().FullName);
        }
        public override string Method1()
        {
            return "这是从第二个插件里面来的数据";
        }

        public override void Unload()
        {
            Console.WriteLine("Unload" + this.GetType().FullName);
        }
    }
}
