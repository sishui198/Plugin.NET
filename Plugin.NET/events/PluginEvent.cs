﻿namespace PluginNET.events
{
    /// <summary>
    /// 插件加载事件
    /// </summary>
    /// <param name="e">事件参数</param>
    public delegate void PluginEvent(PluginEventArgs e);
}
